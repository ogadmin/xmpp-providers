#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
This script validates the URLs in the Markdown and JSON files.
"""

from collections import defaultdict
import sys
import re
from urllib.parse import unquote
import requests

from common import *

# URLs containing those parts are not checked.
EXCLUDED_URL_PARTS = [
	"example.org",
	"<version>",
]

# Redirects are allowed for URLs with those prefixes.
REDIRECTED_URL_PREFIXES = [
	"https://invent.kde.org/melvo/xmpp-providers/-/jobs/artifacts/master/",
	"https://jabber.at/api/set-lang/?lang=",
]

# Set "logging.DEBUG" for debug output.
LOG_LEVEL = logging.INFO

logging.basicConfig(level=LOG_LEVEL, format="%(levelname)-8s %(message)s")

urls = defaultdict(lambda: defaultdict(list))
urls_invalid = False

for file_path in README_FILE_PATH, CONTRIBUTING_FILE_PATH, PROVIDERS_FILE_PATH, CLIENTS_FILE_PATH:
	with open(file_path, "r") as file:
		line_number = 0

		for line in file:
			line_number += 1
			url_matches = re.findall("https://[^\s\"]*?\)|https://[^\s\)]*?\"|https://[^\"\;)]*?\s|https://[^\s\"\)]*?;", line)

			for url_match in url_matches:
				url = url_match[:-1]
				url_included = True

				# Exclude specific URLs from the check.
				for excluded_url_part in EXCLUDED_URL_PARTS:
					if url.find(excluded_url_part) != -1:
						logging.debug("Excluding from check %s:%s:\t%s because it contains '%s'" % (file_path, line_number, url, excluded_url_part))
						url_included = False
						break

				if url_included:
					logging.debug("Inserting for check %s:%s:\t%s" % (file_path, line_number, url))
					urls[url][file_path].append(line_number)

for url, occurrences in urls.items():
	logging.debug("Checking %s in files / lines: %s" % (url, json.dumps(occurrences)))

	try:
		redirects_allowed = False

		# Allow redirects only for specific URLs.
		for redirected_url_prefix in REDIRECTED_URL_PREFIXES:
			if url.startswith(redirected_url_prefix):
				redirects_allowed = True
				break

		# Request the URL.
		# Percent-encoded URLs are decoded by "unquote()" because the GET request expects them decoded.
		request = requests.get(unquote(url), allow_redirects=redirects_allowed)

		status_code = request.status_code
		logging.debug("Received status code: %s" % status_code)

		if status_code != 200:
			logging.error("%s: INVALID status code %s in files / lines: %s" % (url, status_code, json.dumps(occurrences)))
			urls_invalid = True
	except requests.RequestException as e:
		logging.error("Request failed for %s in files / lines: %s: %s" % (url, json.dumps(occurrences), str(e)))
		sys.exit(1)

if urls_invalid:
	sys.exit(1)
